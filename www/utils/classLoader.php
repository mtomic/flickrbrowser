<?php
/**
 * Class loader that auto loads classes and replaces the underscore path notation with the proper directory path.
 * @package utils
 * @author Marko Tomic <marko@markomedia.com.au>
 * @copyright Copyright 2012 Marko Tomic
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * This file is part of FlickrBrowser.
 *
 *   FlickrBrowser is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FlickrBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FlickrBrowser.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Auto loader method
 * @param $className
 * @return void
 */
function __autoload($className) {
	$file = str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    if(!file_exists($file)) {
		return false;
    } else {
		require_once $file;
	}
}

$controller = new controllers_Controller();

?>