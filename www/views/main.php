<?php
/**
 * @author Marko Tomic <marko@markomedia.com.au>
 * @copyright Copyright 2012 Marko Tomic
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version 1.0.0
 * This file is part of FlickrBrowser.
 *
 *   FlickrBrowser is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FlickrBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FlickrBrowser.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<div class="container">
	<form id="searchForm" method="GET" action="/?action=main">
		<input type="text" name="searchtext" id="searchtext" placeholder="Search Flickr" value="<?php echo $this->searchtext ?>" />
		<input type="submit" name="query" value="Search" />
	</form>
	<div class="message"><?php echo $this->message; ?></div>
	<ul class="imagelist">
		<?php 
		foreach($this->model->images as $image) {
			echo '<li><a href="/?action=detail&fm=' . $image->getFarm() . '&sr=' . $image->getServer() . '&id=' . $image->getId() . '&sc=' . $image->getSecret() . '"><img src="' . $image->getThumbSrc() . '" alt="' . $image->getTitle() . '" title="' . $image->getTitle() . '" /></a><br/><div class="thumb-title">' . substr($image->getTitle(), 0, 30) . '</div></li>';
		}	
		?>
	</ul>
	<div class="pagination"><?php echo $this->pagination; ?></div>
</div>