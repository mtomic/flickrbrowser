<?php
/**
 * Image class Interface
 * @package models
 * @author Marko Tomic <marko@markomedia.com.au>
 * @copyright Copyright 2012 Marko Tomic
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * This file is part of FlickrBrowser.
 *
 *   FlickrBrowser is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FlickrBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FlickrBrowser.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
*	interface iImage
*/
interface iImage {
   /**
	* Class constructor
	* @return void
	*/
	public function __construct();

	/**
	* Getter - for image id
	* @return $this->id
	*/
	public function getId();

	/**
	* Setter - for Image id
	* @param $value
	* @return void
	*/
	public function setId($value);

	/**
	* Getter - for image owner
	* @return $this->owner
	*/
	public function getOwner();

	/**
	* Setter - for image owner
	* @param $value
	* @return void
	*/
	public function setOwner($value);

	/**
	* Getter - for image title
	* @return $this->title
	*/
	public function getTitle();

	/**
	* Setter - for image title
	* @param $value
	* @return void
	*/
	public function setTitle($value);

	/**
	* Getter - for image public boolean value
	* @return $this->isPublic
	*/
	public function getIsPublic();

	/**
	* Setter - for public boolean value
	* @param $value
	* @return void
	*/
	public function setIsPublic($value);

	/**
	* Getter - for image thumbnal link
	* @return $this->thumbSrc
	*/
	public function getThumbSrc();

	/**
	* Setter - for image thumbnail link
	* @param $value
	* @return void
	*/
	public function setThumbSrc($value);

	/**
	* Getter - for image large link
	* @return $this->originalSrc
	*/
	public function getOriginalSrc();

	/**
	* Setter - for image large link
	* @param $value
	* @return void
	*/
	public function setOriginalSrc($value);

	/**
	* Getter - for image farm
	* @return $this->farm
	*/
	public function getFarm();

	/**
	* Setter - for image farm
	* @param $value
	* @return void
	*/
	public function setFarm($value);

	/**
	* Getter - for image server
	* @return $this->server
	*/
	public function getServer();

	/**
	* Setter - for image server
	* @param $value
	* @return void
	*/
	public function setServer($value);

	/**
	* Getter - for image secret
	* @return $this->secret
	*/
	public function getSecret();

	/**
	* Setter - for image secret
	* @param $value
	* @return void
	*/
	public function setSecret($value);

	/**
	* Class destructor
	* @return void
	*/
	public function __destruct();
}
?>