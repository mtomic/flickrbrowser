<?php
/**
 * Model class that stores all Image info
 * @package models
 * @author Marko Tomic <marko@markomedia.com.au>
 * @copyright Copyright 2012 Marko Tomic
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * This file is part of FlickrBrowser.
 *
 *   FlickrBrowser is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FlickrBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FlickrBrowser.  If not, see <http://www.gnu.org/licenses/>.
 */
require('iImage.php');
/**
 * class Image
 * @package models
 */
class models_Image implements iImage {
	/**
    * Image id
    * @var mixed
    */
	protected $id;
	
	/**
    * Image Owner
    * @var mixed
    */
	protected $owner;
	
	/**
    * Image Title
    * @var mixed
    */
	protected $title;

	/**
    * Is image public
    * @var boolean
    */
	protected $isPublic;
	
	/**
    * Image Thumbnail link
    * @var mixed
    */
	protected $thumbSrc;
	
	/**
    * Image large link
    * @var mixed
    */
	protected $originalSrc;
	
	/**
    * Image Farm
    * @var mixed
    */
	protected $farm;
	
	/**
    * Image server
    * @var mixed
    */
	protected $server;

	/**
    * Image Secret
    * @var mixed
    */
	protected $secret;

	/**
	* Class constructor
	* @return void
	*/
	public function __construct() {

	}

	/**
	* Getter - for image id
	* @return $this->id
	*/
	public function getId() { 
		return $this->id; 
	} 

	/**
	* Setter - for Image id
	* @param $value
	* @return void
	*/
	public function setId($value) { 
		$this->id = $value; 
	} 

	/**
	* Getter - for image owner
	* @return $this->owner
	*/
	public function getOwner() { 
		return $this->owner; 
	} 

	/**
	* Setter - for image owner
	* @param $value
	* @return void
	*/
	public function setOwner($value) { 
		$this->owner = $value; 
	} 

	/**
	* Getter - for image title
	* @return $this->title
	*/
	public function getTitle() { 
		return $this->title; 
	} 

	/**
	* Setter - for image title
	* @param $value
	* @return void
	*/
	public function setTitle($value) { 
		$this->title = $value; 
	} 

	/**
	* Getter - for image public boolean value
	* @return $this->isPublic
	*/
	public function getIsPublic() { 
		return $this->isPublic; 
	} 

	/**
	* Setter - for public boolean value
	* @param $value
	* @return void
	*/
	public function setIsPublic($value) { 
		$this->isPublic = $value; 
	} 

	/**
	* Getter - for image thumbnal link
	* @return $this->thumbSrc
	*/
	public function getThumbSrc() { 
		return $this->thumbSrc; 
	} 

	/**
	* Setter - for image thumbnail link
	* @param $value
	* @return void
	*/
	public function setThumbSrc($value) { 
		$this->thumbSrc = $value; 
	}

	/**
	* Getter - for image large link
	* @return $this->originalSrc
	*/
	public function getOriginalSrc() { 
		return $this->originalSrc; 
	} 

	/**
	* Setter - for image large link
	* @param $value
	* @return void
	*/
	public function setOriginalSrc($value) { 
		$this->originalSrc = $value; 
	}

	/**
	* Getter - for image farm
	* @return $this->farm
	*/
	public function getFarm() { 
		return $this->farm; 
	} 

	/**
	* Setter - for image farm
	* @param $value
	* @return void
	*/
	public function setFarm($value) { 
		$this->farm = $value; 
	}

	/**
	* Getter - for image server
	* @return $this->server
	*/
	public function getServer() { 
		return $this->server; 
	} 

	/**
	* Setter - for image server
	* @param $value
	* @return void
	*/
	public function setServer($value) { 
		$this->server = $value; 
	}

	/**
	* Getter - for image secret
	* @return $this->secret
	*/
	public function getSecret() { 
		return $this->secret; 
	} 

	/**
	* Setter - for image secret
	* @param $value
	* @return void
	*/
	public function setSecret($value) { 
		$this->secret = $value; 
	}

	/**
	* Class destructor
	* @return void
	*/
	public function __destruct() {
		
	}
}
?>