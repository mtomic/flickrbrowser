<?php
/**
 * Model class that simulates common Flickr API calls
 * @package models
 * @author Marko Tomic <marko@markomedia.com.au>
 * @copyright Copyright 2012 Marko Tomic
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * This file is part of FlickrBrowser.
 *
 *   FlickrBrowser is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FlickrBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FlickrBrowser.  If not, see <http://www.gnu.org/licenses/>.
 */
require('iFlickr.php');
/**
 * class Flickr
 * @package models
 */
class models_Flickr implements iFlickr {

	/**
    * Expected format for Flickr API calls
    * @var mixed
    */
	protected $format = "json";
	
	/**
    * API Key
    * @var mixed
    */
	protected $api_key;
	
	/**
    * API End Point
    * @var mixed
    */
	protected $endpoint;
	
	/**
    * Number of images per page
    * @var mixed
    */
	protected $per_page;
	
	/**
    * Total number of images
    * @var mixed
    */
	protected $total;
	
	/**
    * Total number of pages
    * @var mixed
    */
	protected $pages;
	
	/**
    * Default current page
    * @var mixed
    */
	protected $page = 1;

	/**
	*	main constructor
	*	@return void	
	*/
	public function __construct() {

	}	

	/**
	* Sends API call to Flickr to search images
	* @param $text text to search for on Flickr
	* @param $page page to retrieve from the resultset
	* @return $result result is encoded into php native object
	*/
	public function search($text, $page) {
		$result = "";
		$request_url = $this->getEndpoint() . "?method=flickr.photos.search&api_key=" . $this->getApiKey() . "&format=" . $this->getFormat() . "&text=" . $text . "&per_page=" . $this->getPerPage() . "&page=" . $page . "&nojsoncallback=1";
		$result = file_get_contents($request_url);
		return json_decode($result);
	}

	/**
	* Getter - for api response format
	* @return $this->format
	*/
	public function getFormat() { 
		return $this->format; 
	} 

	/**
	* Getter - for api key
	* @return $this->api_key
	*/
	public function getApiKey() { 
		return $this->api_key; 
	} 

	/**
	* Getter - for api endpoint
	* @return $this->endpoint
	*/
	public function getEndpoint() { 
		return $this->endpoint; 
	} 

	/**
	* Setter - for api response format
	* @param $value
	* @return void
	*/
	public function setFormat($value) { 
		$this->format = $value; 
	} 

	/**
	* Setter - for api key
	* @param $value
	* @return void
	*/
	public function setApiKey($value) { 
		$this->api_key = $value; 
	} 

	/**
	* Setter - for api endpoint
	* @param $value
	* @return void
	*/
	public function setEndpoint($value) { 
		$this->endpoint = $value; 
	}

	/**
	* Getter - for per-page setting
	* @return $this->per_page
	*/
	public function getPerPage() { 
		return $this->per_page; 
	} 

	/**
	* Setter - for per-page setting
	* @param $value
	* @return void
	*/
	public function setPerPage($value) { 
		$this->per_page = $value; 
	}  

	/**
	* Getter - for total number of images
	* @return $this->total
	*/
	public function getTotalImages() { 
		return $this->total; 
	} 

	/**
	* Getter - for total number of pages
	* @return $this->pages
	*/
	public function getPages() { 
		return $this->pages; 
	} 

	/**
	* Getter - for current page
	* @return $this->page
	*/
	public function getPage() { 
		return $this->page; 
	} 

	/**
	* Setter - for total number of images
	* @param $value
	* @return void
	*/
	public function setTotalImages($value) { 
		$this->total = $value; 
	} 

	/**
	* Setter - for total number of pages
	* @param $value
	* @return void
	*/
	public function setPages($value) { 
		$this->pages = $value; 
	} 

	/**
	* Setter - for the current page
	* @param $value
	* @return void
	*/
	public function setPage($value) { 
		$this->page = $value; 
	} 

	/**
	* Class destructor
	* @return void
	*/
	public function __destruct() {
		// Class destructor
	}
}
?>