<?php
/**
 * Flickr class Interface
 * @package models
 * @author Marko Tomic <marko@markomedia.com.au>
 * @copyright Copyright 2012 Marko Tomic
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * 
 * This file is part of FlickrBrowser.
 *
 *   FlickrBrowser is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FlickrBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FlickrBrowser.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
*	interface iFlickr
*/
interface iFlickr {
   /**
	*	main constructor
	*	@return void	
	*/
	public function __construct();	

	/**
	* Sends API call to Flickr to search images
	* @param $text text to search for on Flickr
	* @param $page page to retrieve from the resultset
	* @return $result result is encoded into php native object
	*/
	public function search($text, $page);

   /**
	* Getter - for api response format
	* @return $this->format
	*/
	public function getFormat();

	/**
	* Getter - for api key
	* @return $this->api_key
	*/
	public function getApiKey();

	/**
	* Getter - for api endpoint
	* @return $this->endpoint
	*/
	public function getEndpoint();

	/**
	* Setter - for api response format
	* @param $value
	* @return void
	*/
	public function setFormat($value);

	/**
	* Setter - for api key
	* @param $value
	* @return void
	*/
	public function setApiKey($value);

	/**
	* Setter - for api endpoint
	* @param $value
	* @return void
	*/
	public function setEndpoint($value);

	/**
	* Getter - for per-page setting
	* @return $this->per_page
	*/
	public function getPerPage();

	/**
	* Setter - for per-page setting
	* @param $value
	* @return void
	*/
	public function setPerPage($value);

	/**
	* Getter - for total number of images
	* @return $this->total
	*/
	public function getTotalImages();

	/**
	* Getter - for total number of pages
	* @return $this->pages
	*/
	public function getPages();

	/**
	* Getter - for current page
	* @return $this->page
	*/
	public function getPage();

	/**
	* Setter - for total number of images
	* @param $value
	* @return void
	*/
	public function setTotalImages($value);

	/**
	* Setter - for total number of pages
	* @param $value
	* @return void
	*/
	public function setPages($value);

	/**
	* Setter - for the current page
	* @param $value
	* @return void
	*/
	public function setPage($value);

	/**
	* Class destructor
	* @return void
	*/
	public function __destruct();
}
?>