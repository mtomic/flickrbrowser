<?php
/**
 * @author Marko Tomic <marko@markomedia.com.au>
 * @copyright Copyright 2012 Marko Tomic
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version 1.0.0
 * This file is part of FlickrBrowser.
 *
 *   FlickrBrowser is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FlickrBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FlickrBrowser.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Flickr Browser</title>
<link rel="stylesheet" type="text/css" href="css/main.css" media="all" />
</head>
<body>