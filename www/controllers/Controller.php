<?php
/**
 * This is the main controller for the app.
 * @package controllers
 * @author Marko Tomic <marko@markomedia.com.au>
 * @copyright Copyright 2012 Marko Tomic
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * This file is part of FlickrBrowser.
 *
 *   FlickrBrowser is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FlickrBrowser is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FlickrBrowser.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 *	main controller class
 * 	@package controllers
 */
class controllers_Controller {
	/**
    * Image text to search for
    * @var mixed
    */
	protected $searchtext = "";
	
	/**
    * action to invoke in the controller
    * @var mixed
    */
	protected $action = "main";
	
	/**
    * Current page defaults to 1
    * @var mixed
    */
	protected $page = 1;

	/**
    * Pagination content
    * @var mixed
    */
	protected $pagination = "";
	
	/**
    * Message to be displayed if somehting goes wrong.
    * @var mixed
    */
	protected $message = "";

	/**
    * Controller constructor
    */
	public function __construct() {
		$this->model->flickr = new models_Flickr();
		$this->model->images = array();
	}

	/**
    * Invokes the main controller
    * @return void
    */
	public function invoke() {
		if(isset($_GET['page']) and is_numeric($_GET['page'])) {
			$this->page = $_GET['page'];
		}

		if(isset($_GET['action'])) {
			$this->action = $_GET['action'];
		} 

		if(isset($_GET['searchtext'])) {
			$this->searchtext = $_GET['searchtext'];
			$this->actionSearch();
		}

		switch($this->action) {
			case "main" :
				include('views/main.php');
			break;
			case "detail" :
				$this->actionDetail();
				include('views/detail.php');
			break;
			default;
				include('views/404.php');
			break;
		}
	}

	/**
    * Called when searching Flickr
    * @return void
    */
	private function actionSearch() {
		global $api_key;
		global $api_endpoint;
		global $per_page;

		$this->searchtext = $_GET['searchtext'];
		//we have something to search for, let's do it
		$this->model->flickr->setApiKey($api_key);
		$this->model->flickr->setEndpoint($api_endpoint);
		$this->model->flickr->setPerPage($per_page);
		$result = $this->model->flickr->search(urlencode($this->searchtext), $this->page);

		if($result->stat == 'ok') {
			if($result->photos->total > 0) {	
				$this->model->flickr->setTotalImages($result->photos->total);
				$this->model->flickr->setPages($result->photos->pages);
				$this->model->flickr->setPage($result->photos->page);
				$this->model->flickr->setTotalImages($result->photos->total);

				foreach ($result->photos->photo as $photo) {
					$this->model->image = new models_Image();
					$this->model->image->setId($photo->id);
					$this->model->image->setOwner($photo->owner);
					$this->model->image->setTitle($photo->title);
					$this->model->image->setIsPublic($photo->ispublic);
					$this->model->image->setFarm($photo->farm);
					$this->model->image->setServer($photo->server);
					$this->model->image->setSecret($photo->secret);
					$thumbSrc = $this->buildThumbURL($photo->farm, $photo->server, $photo->id, $photo->secret);
					$originalSrc = $this->buildOriginalURL($photo->farm, $photo->server,  $photo->id, $photo->secret);
					$this->model->image->setThumbSrc($thumbSrc);
					$this->model->image->setOriginalSrc($originalSrc);
					$this->model->images[] = $this->model->image;
				}

				$this->setPagination();
			} else {
				$this->message = "No images found for <em>" . $this->searchtext . "</em>";
			}
		} else if(!strlen(trim($this->searchtext))) {
			$this->message = "Please enter your search criteria";
		} else {
			$this->message = "Could not connect to Flickr. Please try again later.";
		}
	}

	/**
    * Called when viewing a large image
    * @return void
    */
	private function actionDetail() {
		$this->model->image = new models_Image();
		$this->model->image->setFarm($_GET['fm']);
		$this->model->image->setServer($_GET['sr']);
		$this->model->image->setId($_GET['id']);
		$this->model->image->setSecret($_GET['sc']);
		$originalSrc = $this->buildOriginalURL($this->model->image->getFarm(), $this->model->image->getServer(), $this->model->image->getId(), $this->model->image->getSecret());
		$this->model->image->setOriginalSrc($originalSrc);
	}

	/**
    * Sets up the pagination
    * @return void
    */
	private function setPagination() {
		$this->pagination = ""; 
		if($this->model->flickr->getPage() > 1) {
			$this->pagination .= "<a href='/?searchtext=" . $this->searchtext . "&page=" . ($this->model->flickr->getPage()-1) . "'>Previous</a> ";			
		}
		
		if($this->model->flickr->getPages() > 0) {
			$this->pagination .= " " . $this->model->flickr->getPage() . " of " . $this->model->flickr->getPages(); 
		}

		if($this->model->flickr->getPage() < $this->model->flickr->getPages()) {
			$this->pagination .= " <a href='/?searchtext=" . $this->searchtext . "&page=" . ($this->model->flickr->getPage()+1) . "'>Next</a>";			
		}
	}

	/**
    * Sets up a flickr Thumbnail URL
    * @param $farm
    * @param $server
    * @param $id
    * @param $secret
    * @return $src
    */
	private function buildThumbURL($farm, $server, $id, $secret) {
		$src = "http://farm" . $farm . ".static.flickr.com/" . $server . "/" . $id . "_" . $secret . "_t" . ".jpg";
		return $src;
	}

	/**
    * Sets up a flickr large image URL
    * @param $farm
    * @param $server
    * @param $id
    * @param $secret
    * @return $src
    */
	private function buildOriginalURL($farm, $server, $id, $secret) {
		$src = "http://farm" . $farm . ".static.flickr.com/" . $server . "/" . $id . "_" . $secret . "_b" . ".jpg";
		return $src;
	}
}
?>