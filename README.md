## flickrbrowser @version 1.0.0
# Description 
Flickr image gallery, developed in PHP and plain HTML. The user is be able to enter
a keyword, which is then used to search Flickr. The search results are paginated
and displayed as five results per page, and the user is able to navigate to
other pages. Each image is displayed as a thumbnail; clicking on the thumbnail
opens a new page which shows the full-size image. This application works efficiently, 
no matter how many images match the keyword. It only fetches the current page from Flickr.

# Instructions
* add your Flickr API Key in www/config/config.php
* place contents of www folder in your webroot

Here is a sample Apache vhost for this app:
```
<VirtualHost *:80>
        ServerName flickr.dev
        DocumentRoot /path_to_project/www
        DirectoryIndex index.php
</VirtualHost>
```

* Point your browser to http://flickr.dev

#Notes
This application follows a traditional MVC pattern with Test Driven Development in mind.
Business logic is clearly separated from the presentation layer. The main Controller contains all of the business logic, whereas the presentation layer is in 'views' directory. The view is deliberately plain and easily customisable.  No third party frameworks have been used in this application, apart from PHPUnit for Unit Testing and phpDocumentor for Documentation.  The code is 100% my own.

#Unit Tests
I've written some Unit Tests in 'tests' directory and haven't finished it.  This is work in progress.  To run a test, download the PHPUnit archive (phar):
```
wget http://pear.phpunit.de/get/phpunit.phar
```
```
chmod +x phpunit.phar
```
Then run Unit Tests like so:
```
./phpunit.phar FlickrTest.php
```

#Demo
A working example of this little widget can be seen here:  
http://flickr.markomedia.com.au  
  
Documentation here:  
http://flickr.markomedia.com.au/docs