<?php
/**
 * PHPUnit class that simulates common Flickr API calls
 * @package tests
 * @author Marko Tomic <marko@markomedia.com.au>
 * @copyright Copyright 2012 Marko Tomic
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
set_include_path('../www');
require('models/Flickr.php');

/**
 * FlickrTest is a PHPUnit test class
 */
class FlickrTest extends PHPUnit_Framework_TestCase {
	
	/**
	 * Sets up FlickrTest collection
	 */
	protected function setUp() {
		require('config/config.php');
		$this->flickr = new models_Flickr();
		$this->flickr->setApiKey($api_key);
		$this->flickr->setEndpoint($api_endpoint);
		$this->flickr->setPerPage($per_page);
	}

	/**
	 * testAPIKey PHPUnit Test Case
	 */
	public function testAPIKey() {
		$result = $this->flickr->search(urlencode("shark"), 1, $this->flickr->getPerPage());
		$this->assertTrue($result->stat == 'ok');
	}

	/**
	 * testPageNotFound PHPUnit Test Case
	 */
	public function testPageNotFound() {
		$result = $this->flickr->search(urlencode("rgreeerg reg regrererrfge reg greg ergg erg reger gregergre gregre"), 10, $this->flickr->getPerPage());
		$this->assertTrue($result->photos->pages == "0");
	}
}
?>